
# iSentia Coding Challenge Task

This repository contains a response to Hivery's backend challenge.

The source is licenced under GPL3.

The demo can be executed in a virtual machine (provided) or in an existing environment that has stock install of python3, mongodb and virtualenv.

# TL;DR

## Install Vagrant

Get vagrant from [https://www.vagrantup.com](https://www.vagrantup.com/downloads.html). It is free and allow scripted creation of virtual machines.

## Fire up the VM and start the dev server
```
vagrant up
# wait for the server to get provisioned
make ssh
cd workdir/backend
make dev
```

## Do some crawling on bbc.com
From another terminal window, leave the server running in the first one.
```
make ssh
cd workdir/backend
make crawl
```

This will start the scrapy crawler and import pages from bbc.com to mongodb.

The imported articles are parsed using `rake_nltk` to get a list of weighted keywords. These are also stored in mongodb.

## Checking the API
Navigate to [http://localhost:8080/docs](http://localhost:8080/docs) to load the [Swagger-UI](https://swagger.io/tools/swagger-ui/) with the API documentation and a UI to test it.

In case it does not load, please create the port forwards manually. Open up a third terminal window and do:
```
make portforwards
```

Refresh the browser. A [Swagger-UI](https://swagger.io/tools/swagger-ui/) should load up with the API documentation.

## Listing and searching articles

- All articles can be listed by the /articles endpoint.

## Searching articles

- A proper search can be made by hitting /search?q=keywords

The search will first look in Keywords matches, and return matches with the highest keyword weight first. 

If nothing found with Keywords, it will perform a full-text search within the raw article bodies.


# Stage 1. Setup using Vagrant

If you don't want to use vagrant, please go to `Stage 2` (see below)

## Prerequisities

- Vagrant: Get vagrant from [https://www.vagrantup.com](https://www.vagrantup.com/downloads.html). It is free and allow scripted creation of virtual machines.


## Installation

## 1. Clone this repository

## 2. Bring up the VM

Execute `vagrant up` to bring up the VM.

### Errors during startup

It might complain about IP addresses and network interfaces. Please check the following lines in the Vagrantfile:

```
config.vm.network "private_network", ip: "192.168.33.33"
config.vm.network "public_network", bridge: 'en4: Thunderbolt Ethernet'
```

### Provisioning process

The `vagrant up` command will:

1. Create a brand new fedora based on the minimalistic fedora 28 cloud base image. It will download about 300MB in the first run.

2. Provision the virtual machine, installing and configuring all required packages. It will download another 300MB. It will do this every time you re-create the VM.

3. Copy the contents of the repository to /home/dev in the VM

4. Execute `make deps` on the backend, which will create virtualenv and install all depenencies.

### Re-run the provisioner

In case something fails, you can always do:

`vagrant reload --provision` to re-run the whole vm setup.

You can also run parts of the process:

### To only run the provisioner (withouth restarting the vm):

```
vagrant provision
```

## Editing files in the VM

The repository is copied to `/home/dev` inside the VM. This means that file changes in the host will **NOT** be picked up by the developer server running inside the VM.

Please mount the VM's filesytem using in order to edit the live sources.

On macOS, Press CMD+K in any Finder window. Type `nfs://192.168.33.10` into the box. Live sources are located in `/home/dev/workdir`.


# Stage 2: Setup the backend project (inside or outside of the VM)

## 1. Setup the python environment

NOTE: In case you have used Vagrant this step is already done.

```
cd backend
make deps
```

See more details, trouble shooting in `backend/README.md` file.

## 2. Start the server

If you have used Vagrant, `ssh` into the VM:

```
make ssh
# or using the full command:
vagrant ssh -c "sudo su - dev"
```

Goto the backend directory and execute the server:

```
cd workdir/backend
make dev
```


## 3. Check if the server is running

Open a browser to [http://localhost:8080/docs](http://localhost:8080/docs)

A [Swagger-UI](https://swagger.io/tools/swagger-ui/) should load up displaying the documentation for the available endpoints in the backend.


### In case the VM is used and nothing happens after hitting 8080 

This it means that the port forwarding in Vagrant does not work. Unfortunately, it got quite buggy on macOS recently.

Please issue the following command on the host to create the port forwards manually:

```
make portforwards
# or using the full command
vagrant ssh -- -L8080:127.0.0.1:8080 -L8081:127.0.0.1:8081
```


## 4. Next steps

Please see what the backend can do in the backend's readme file: `backend/README.md`



