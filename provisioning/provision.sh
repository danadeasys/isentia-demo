#!/bin/bash
# -----------------------------------------------------------------------------
# description: General quick dirty provisioner for Fedora
# author: Daniel Kovacs <mondomhogynincsen@gmail.com>
# licence: GPL3 <https://opensource.org/licenses/GPL3>
# version: 1.15
# -----------------------------------------------------------------------------

set -e 

# -----------------------------------------------------------
# Debugging
# -----------------------------------------------------------
#
# Uncomment the following line to debug the provisioner script
#

# set -v


# -----------------------------------------------------------
# load config
# -----------------------------------------------------------

. /vagrant/provisioning/config.sh


# -----------------------------------------------------------
# packages
# -----------------------------------------------------------

echo "$0: installing general packages"

PACKAGES=(
    htop
    net-tools
    npm
    python-devel
    git
    tig
    gcc
)

dnf install -y ${PACKAGES[*]}


# -----------------------------------------------------------
# timezone
# -----------------------------------------------------------

echo "$0: setting date and time"

# timedatectl set-timezone UTC
timedatectl set-timezone Australia/Sydney
timedatectl set-ntp true


# -----------------------------------------------------------
# nfs server
# -----------------------------------------------------------

. /vagrant/provisioning/nfs.sh


# -----------------------------------------------------------
# python environment
# -----------------------------------------------------------

echo "$0: configuring python environment"

pip install virtualenv


# -----------------------------------------------------------
# nodejs environment
# -----------------------------------------------------------

echo "$0: configuring node environment"

# mkdir -p /home/vagrant/envdir/node_modules
# ln -s ../envdir/node_modules /home/vagrant/workdir/node_modules
# chown -R /home/vagrant/envdir


# -----------------------------------------------------------
# mongodb server
# -----------------------------------------------------------

. /vagrant/provisioning/devuser.sh

# -----------------------------------------------------------
# mongodb server
# -----------------------------------------------------------

. /vagrant/provisioning/mongodb.sh

# -----------------------------------------------------------
# project specific setup
# -----------------------------------------------------------

. /vagrant/provisioning/project.sh

