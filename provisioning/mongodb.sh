#!/bin/bash

# Mongo db setup file

set -e

echo "$0: installing mongdb"

# -----------------------------------------------------------
# mongodb
# -----------------------------------------------------------

cat > /etc/yum.repos.d/mongodb-org-4.0.repo <<EOF
[Mongodb]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/amazon/2013.03/mongodb-org/4.0/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.0.asc
EOF

dnf install -y mongodb-org

echo "$0: starting mongdb service "

systemctl enable mongod
systemctl start mongod

# su - vagrant ./mongotron.sh


echo "$0: mongodb ready"
