

# -----------------------------------------------------------
# dev user creation
# -----------------------------------------------------------


echo "$0: setting up developer user ${DEVUSER_NAME}"
if [ ! -d /home/${DEVUSER_NAME} ]; then
    useradd -u ${DEVUSER_UID} -g ${DEVUSER_GID} ${DEVUSER_NAME}
    echo '${DEVUSER_NAME}	ALL=(ALL:ALL) ALL' >> /etc/sudoers
else
	echo "warning: dev user already exists, skipping creation" >&2
fi
echo "${DEVUSER_NAME}:${DEVUSER_PASSWORD}" | chpasswd

# -----------------------------------------------------------
# ssh keys
# -----------------------------------------------------------

echo "$0: setting up ssh keys"

mkdir -p /home/${DEVUSER_NAME}/.ssh

if [ -d /vagrant/keys ]; then
    echo "$0: copying ssh keys..."
    cp -v /vagrant/keys/* /home/${DEVUSER_NAME}/.ssh/
else
    echo "$0: warning: ssh keys are not found in the /vagrant/keys directory. Pushing to git will not be possible." >&2
fi

chown -R ${DEVUSER_NAME}:${DEVUSER_GID} /home/${DEVUSER_NAME}/.ssh
chmod 600 /home/${DEVUSER_NAME}/.ssh/*


echo "$0: executing personalization with devuser"


# -----------------------------------------------------------
# invoking unprivilaged provisioner
# -----------------------------------------------------------

su - ${DEVUSER_NAME} /vagrant/provisioning/devuser-unprivileged.sh
