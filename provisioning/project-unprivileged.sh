#!/bin/bash

set -e


# -----------------------------------------------------------
# load config
# -----------------------------------------------------------

. /vagrant/provisioning/config.sh


# -----------------------------------------------------------
# setup project
# -----------------------------------------------------------

echo "$0: setting up project"

cd ${DEVUSER_WORKDIR}/backend

make deps deps-test deps-build

echo "$0: project ready"
