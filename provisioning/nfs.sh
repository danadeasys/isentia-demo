#!/bin/bash


echo "$0: installing nfs server"

dnf -y install nfs-utils


echo "$0: configuring nfs server"

sed -i -r "s/#Domain = [a-z.]*/Domain = devbox/g" /etc/idmapd.conf

cat > /etc/exports <<EOF

/ *(rw,insecure,nohide,no_root_squash)

EOF

echo "$0: starting nfs server"

systemctl start rpcbind nfs-server
systemctl enable rpcbind nfs-server

echo "$0: nfs server ready"
