#!/bin/bash


# -----------------------------------------------------------
# workdir setup
# -----------------------------------------------------------


if [ ! -d ${DEVUSER_WORKDIR} ]; then
    echo "$0: echo copying workfiles to ${DEVUSER_WORKDIR}"
    rsync -rt /vagrant /home/${DEVUSER_NAME}/
    mv /home/${DEVUSER_NAME}/vagrant ${DEVUSER_WORKDIR}
else
    echo "$0: warning: working directory already exist, skipping copying: ${DEVUSER_WORKDIR}" >&2
fi

chown -R ${DEVUSER_NAME}:${DEVUSER_GID} ${DEVUSER_WORKDIR}


# -----------------------------------------------------------
# invoking unprivilaged provisioner
# -----------------------------------------------------------

su - ${DEVUSER_NAME} /vagrant/provisioning/project-unprivileged.sh

