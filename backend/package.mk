# -----------------------------------------------------------------------------
# description: Package Specific Makefile
# licence: GPL3 <https://opensource.org/licenses/GPL3>
# author: Daniel Kovacs <mondomhogynincsen@gmail.com>
# version: 0.1
# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# declarations
# -----------------------------------------------------------------------------

# add package specific declarations here

# -----------------------------------------------------------------------------
# flask env vars
# -----------------------------------------------------------------------------


FLASK_APP=isentia.app:app
FLASK_ENV=development 
#FLASK_APP_CONFIG=isentia.config.TestConfig

FLASK_VARS=export FLASK_APP=$(FLASK_APP) && export FLASK_ENV=$(FLASK_ENV)

# -----------------------------------------------------------------------------
# dev
# -----------------------------------------------------------------------------
#
# Start local development server
#

.PHONY: dev
dev:: deps
	source activate && $(FLASK_VARS) && flask run --port 8080


# -----------------------------------------------------------------------------
# shell
# -----------------------------------------------------------------------------

.PHONY: shell
flask-shell:: deps
	source activate && $(FLASK_VARS) && flask shell


# -----------------------------------------------------------------------------
# db-create
# -----------------------------------------------------------------------------
#
# Initialize the local database
#

db-create::
	source activate && flask create_db



# -----------------------------------------------------------------------------
# crawl
# -----------------------------------------------------------------------------
#
# Initialize crawler
#

~/nltk-data:: deps
	source activate && python -c "import nltk; nltk.download('stopwords')"
	touch ~/nltk-data


nltk-deps:: ~/nltk-data


# -----------------------------------------------------------------------------
# crawl
# -----------------------------------------------------------------------------
#
# Initialize crawler
#

crawl:: nltk-deps
	source activate && scrapy crawl bbc.com -o output.json

