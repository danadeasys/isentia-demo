# encoding: utf-8
# author: Daniel Kovacs <mondomhogynincsen@gmail.com>
# licence: GPL3 <https://opensource.org/licenses/GPL3>
# file: shell.py
# purpose: interactive demo
# version: 1.0


# ---------------------------------------------------------------------------------------
# imports
# ---------------------------------------------------------------------------------------

import sys, os
from pprint import pprint
pp = pprint


# ---------------------------------------------------------------------------------------
# package specific imports
# ---------------------------------------------------------------------------------------

from isentia import *


# ---------------------------------------------------------------------------------------
# main
# ---------------------------------------------------------------------------------------

# setup your shell here
# use print() to print help to users
