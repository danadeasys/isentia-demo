# encoding: utf-8
# author: Daniel Kovacs <mondomhogynincsen@gmail.com>
# licence: GPL3 <https://opensource.org/licenses/GPL3>
# file: 
# purpose:
# version:

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# imports
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import os
import pytest
import requests
import json

from sutils import qdict

from . auto_imports import *

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# package imports
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import isentia


# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# DEFAULT_SESSION_CONFIG
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

DEFAULT_SESSION_CONFIG = qdict(
    base_url='http://localhost:8080',
    test_data_dir="../resources",
)


# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# fixture: session_config
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

@pytest.fixture(scope="session")
def session_config():
    config = qdict()
    config.update(DEFAULT_SESSION_CONFIG)
    for key in config.keys():
        config[key] = os.environ.get('TEST_{}'.format(key.upper()), config[key])
    return config


# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# fixture: http_client()
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

@pytest.fixture(scope="session")
def http_client():
    return requests.Session()


# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# fixture: api_client
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

@pytest.fixture(scope="session")
def api_client(session_config):
    from bravado.client import SwaggerClient
    from bravado.requests_client import RequestsClient
    spec_url ='{}{}'.format(session_config.base_url, "/spec.json")
    http_client = RequestsClient()
    return SwaggerClient.from_url(spec_url, http_client=http_client)
    

# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# fixture: initial_data()
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

@pytest.fixture(scope="session")
def initial_data(session_config):
    return qdict(
    )


# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# setup_module()
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

@pytest.fixture(scope="session")
def clean_db(session_config, http_client):
    response = http_client.delete(urljoin(session_config.base_url, '/debug/drop_db'))
    assert response.status_code == 204, 'Failed to drop database'


