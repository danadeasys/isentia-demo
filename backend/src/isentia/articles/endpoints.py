# encoding: utf-8
# package: isentia-demo
# author: Daniel Kovacs <danadeasysau@gmail.com>
# file-version: 1.0

# -------------------------------------------------------------------------------
# imports
# -------------------------------------------------------------------------------

from ..auto_imports import *

from .article import *


# -------------------------------------------------------------------------------
# exports
# -------------------------------------------------------------------------------

__all__ = qlist()

views = ObjectList()
schemas = ObjectList()


# -------------------------------------------------------------------------------
# ArticleSchema
# -------------------------------------------------------------------------------

@schemas.register
class ArticleSchema(ModelSchema):
    class Meta:
        model = Article
        strict = True


# -------------------------------------------------------------------------------
# KeywordSchema
# -------------------------------------------------------------------------------

@schemas.register
class KeywordSchema(ModelSchema):
    class Meta:
        model = Keyword
        strict = True


# -------------------------------------------------------------------------------
# ArticleResource
# -------------------------------------------------------------------------------

@views.register
@doc(tags=['article'])
class ArticleResource(MethodResource):

    BASE_URL = '/article'

    @use_kwargs(
        {
            'id': fields.Integer(required=False),
            'url': fields.Str(required=False),
            # 'date_from': fields.Str(required=False),
            # 'date_to': fields.Str(required=False),
            # 'keywords': fields.List(fields.Str()),
        },
        locations=["query"]
    )
    @marshal_with(ArticleSchema(many=True), code=200)
    @doc()
    def get(self, **filters):
        """Returns article by ID or URL"""
        return Article.objects(**filters).all()

    @use_kwargs(
        {
            'id': fields.Str(required=False),
        }, 
        locations=["query"]
    )
    @marshal_with(None, code=204, description="Deleted")
    @doc()
    def delete(self, **filters):
        """Removes an article by id
        """
        article = Article.objects.get_or_404(**filters)
        article.delete()
        return None, 204



# -------------------------------------------------------------------------------
# KeywordResource
# -------------------------------------------------------------------------------

@views.register
@doc(tags=['article'])
class KeywordResource(MethodResource):

    BASE_URL = '/article/keywords'

    @use_kwargs(
        {
            'id': fields.Str(required=False),
            'url': fields.Str(required=False),
            # 'date_from': fields.Str(required=False),
            # 'date_to': fields.Str(required=False),
            # 'keywords': fields.List(fields.Str()),
        },
        locations=["query"]
    )
    @marshal_with(KeywordSchema(many=True), code=200)
    @doc()
    def get(self, **filters):
        """Returns keywords for an article
        """
        value = filters.pop('url', None)
        if value is not None:
            filters['article__url'] = value
        value = filters.pop('id', None)
        if value is not None:
            filters['article__id'] = value
        return Keyword.objects(**filters).all()


# -------------------------------------------------------------------------------
# ArticleSearchResource
# -------------------------------------------------------------------------------

@views.register
@doc(tags=['article'])
class ArticleSearchResource(MethodResource):

    BASE_URL = '/search'

    @use_kwargs(
        {
            'q': fields.Str(),
        },
        locations=["query"]
    )
    @marshal_with(ArticleSchema(many=True), code=200)
    @doc()
    def get(self, q):
        """Searches articles by keywords or in raw text
        """

        kwd_filters = {}
        # conditions = q.split('\n')
        # for condition in conditions:
        kwd_filters['value__contains'] = q
        kwd_matches = Keyword.objects(**kwd_filters).order_by('-weight').all()

        articles = set([ kwd.article for kwd in kwd_matches ])
        if len(articles) > 0:
            return articles

        articles = Article.objects(raw_body__contains = q).all()

        # article_filters = {}
        # article_filters['id__in']
        # Article.objects(**article_filters).order_by('-keywords__weight').all()



