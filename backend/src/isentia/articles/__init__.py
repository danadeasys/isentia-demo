# encoding: utf-8
# package: isentia-demo
# author: Daniel Kovacs <danadeasysau@gmail.com>
# file-version: 1.0

# -------------------------------------------------------------------------------
# imports
# -------------------------------------------------------------------------------

from sutils import qdict, qlist
from flask import render_template, Blueprint

# -------------------------------------------------------------------------------
# exports
# -------------------------------------------------------------------------------

# __all__ = qlist([''])


# -------------------------------------------------------------------------------
# importing submodules with endpoints
# -------------------------------------------------------------------------------

from .article import *
# from .endpoints import *


