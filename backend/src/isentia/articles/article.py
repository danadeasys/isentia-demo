# encoding: utf-8
# package: isentia-demo
# author: Daniel Kovacs <danadeasysau@gmail.com>
# file-version: 1.0

# -------------------------------------------------------------------------------
# imports
# -------------------------------------------------------------------------------

from datetime import datetime

from ..auto_imports import *


# -------------------------------------------------------------------------------
# exports
# -------------------------------------------------------------------------------

__all__ = qlist()


# -------------------------------------------------------------------------------
# Article
# -------------------------------------------------------------------------------

@__all__.register
class Article(Document):

    # id = StringField(
    #     required=True,
    #     primary_key=True, 
    # )
    
    url = StringField(
        required=True, 
    )

    last_updated = DateTimeField(
        required=False, 
    )

    last_crawled = DateTimeField(
        required=False, 
    )
    
    raw_body = StringField(
        required=True,
    )

    title = StringField(
        required=True,
    )



# -------------------------------------------------------------------------------
# Keyword
# -------------------------------------------------------------------------------

@__all__.register
class Keyword(Document):

    article = ReferenceField(
        Article,
        required=True,
        reverse_delete_rule=CASCADE,
    )

    weight = FloatField(
        required=True,
    )

    value = StringField(
        required=True,
    )

