# encoding: utf-8
# package: isentia-demo
# author: Daniel Kovacs <danadeasysau@gmail.com>
# file-version: 1.0

# -------------------------------------------------------------------------------
# imports
# -------------------------------------------------------------------------------

import os
import logging
from importlib import import_module

from sutils import qdict, qlist

from flask import Flask, render_template, jsonify
from flask_debugtoolbar import DebugToolbarExtension
from flask_mongoengine import MongoEngine
from flask_apispec.extension import FlaskApiSpec
from flask_apispec import wrapper

# from flask_login import LoginManager
# from flask_bcrypt import Bcrypt
# from flask_bootstrap import Bootstrap
# from flask_sqlalchemy import SQLAlchemy
# from flask_migrate import Migrate

from apispec import APISpec
from apispec.ext.flask import FlaskPlugin
from apispec.ext.marshmallow import MarshmallowPlugin


from . import __version__
from . import common

# -------------------------------------------------------------------------------
# exports
# -------------------------------------------------------------------------------

__all__ = qlist(['app', 'db'])


# -------------------------------------------------------------------------------
# Plugin initialization
# -------------------------------------------------------------------------------


# instantiate the extensions
# login_manager = LoginManager()
# bcrypt = Bcrypt()
toolbar = DebugToolbarExtension()
# bootstrap = Bootstrap()
# db = SQLAlchemy()
# migrate = Migrate()
db = MongoEngine()



# -------------------------------------------------------------------------------
# register_module()
# -------------------------------------------------------------------------------

def register_module(app, module_name):
    module = import_module(module_name)
    for schema in module.schemas:
        app.docs.spec.definition(schema.__name__.replace('Schema', ''), schema=schema)
    for view in module.views:
        app.add_url_rule( view.BASE_URL, view_func=view.as_view(view.__name__.lower()) )
        # with app.app_context():
        app.docs.register(view)
        # app.docs.



# -------------------------------------------------------------------------------
# handle_unprocessable_entity()
# -------------------------------------------------------------------------------

def handle_unprocessable_entity(err):
    # webargs attaches additional metadata to the `data` attribute
    exc = getattr(err, "exc")
    if exc:
        # Get validations from the ValidationError object
        messages = exc.messages
    else:
        messages = ["Invalid request"]
    return jsonify({"messages": messages}), 422


# -------------------------------------------------------------------------------
# create_app()
# -------------------------------------------------------------------------------

def create_app(script_info=None):

    # instantiate the app
    app = Flask(
        __name__,
        template_folder='../../../frontend/templates',
        static_folder='../../../frontend/static'
    )

    # set config
    app_settings = os.getenv('FLASK_APP_CONFIG', 'isentia.config.DevelopmentConfig')
    app.config.from_object(app_settings)

    app.config.update({
        'APISPEC_SPEC': APISpec(
            title='isentia',
            version=__version__,
            plugins=[
                # FlaskPlugin(),
                MarshmallowPlugin(),
            ],
        ),
        # 'APISPEC_WEBARGS_PARSER': common
    })

    # Register custom error handler so we can see what is exactly failing at validation.
    app.errorhandler(422)(handle_unprocessable_entity)

    # Add spec handler to app so we don't need to pass it around separately.
    app.docs = FlaskApiSpec(app)

    # set up extensions
    # login_manager.init_app(app)
    # bcrypt.init_app(app)
    toolbar.init_app(app)
    # bootstrap.init_app(app)
    db.init_app(app)
    # migrate.init_app(app, db)

    from .general import blueprint as general_blueprint
    app.register_blueprint(general_blueprint)

    register_module(app, 'isentia.articles.endpoints')

    # # flask login
    # from project.server.models import User
    # login_manager.login_view = 'user.login'
    # login_manager.login_message_category = 'danger'

    # @login_manager.user_loader
    # def load_user(user_id):
    #     return User.query.filter(User.id == int(user_id)).first()

    # # error handlers
    # @app.errorhandler(401)
    # def unauthorized_page(error):
    #     return render_template('errors/401.html'), 401

    # @app.errorhandler(403)
    # def forbidden_page(error):
    #     return render_template('errors/403.html'), 403

    # @app.errorhandler(404)
    # def page_not_found(error):
    #     return render_template('errors/404.html'), 404

    # @app.errorhandler(500)
    # def server_error_page(error):
    #     return render_template('errors/500.html'), 500

    # shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {'app': app, 'db': db}

    return app



# -------------------------------------------------------------------------------
# enable_logging()
# -------------------------------------------------------------------------------

def enable_logging():
    logging.getLogger('webargs.core').setLevel(logging.DEBUG)
    pass




# -------------------------------------------------------------------------------
# initialize the app object
# -------------------------------------------------------------------------------

enable_logging()
app = create_app()
