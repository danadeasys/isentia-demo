# encoding: utf-8
# package: isentia-demo
# author: Daniel Kovacs <danadeasysau@gmail.com>
# file-version: 1.0

# -------------------------------------------------------------------------------
# imports
# -------------------------------------------------------------------------------

from datetime import datetime

from ..articles import *

import scrapy


# -------------------------------------------------------------------------------
# exports
# -------------------------------------------------------------------------------



# -------------------------------------------------------------------------------
# ArticleItem
# -------------------------------------------------------------------------------

class ArticleItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass
