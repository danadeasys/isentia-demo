# encoding: utf-8
# package: isentia-demo
# author: Daniel Kovacs <danadeasysau@gmail.com>
# file-version: 1.0

# -------------------------------------------------------------------------------
# imports
# -------------------------------------------------------------------------------

from datetime import datetime

import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor

from rake_nltk import Rake
import nltk

from ...auto_imports import *
from ...articles import *


# -------------------------------------------------------------------------------
# exports
# -------------------------------------------------------------------------------

__all__ = qlist()


# -------------------------------------------------------------------------------
# BBCSpider
# -------------------------------------------------------------------------------

@__all__.register
class BBCSpider(CrawlSpider):
    name = 'bbc.com'
    allowed_domains = ['bbc.com']
    start_urls = [
        'http://www.bbc.com/news',
        'http://www.bbc.com/news/world',
        'http://www.bbc.com/news/uk',
        'http://www.bbc.com/news/tech',
        'http://www.bbc.com/news/business',
        'http://www.bbc.com/news/science_and_environment',
    ]

    rules = (
        # Extract links matching 'category.php' (but not matching 'subsection.php')
        # and follow links from them (since no callback means follow=True by default).
        # Rule(LinkExtractor(allow=('/', ), )),
        Rule(LinkExtractor(allow=('news/.*', )), callback='parse_item'),
    )

    def parse_item(self, response):
        self.logger.info('====> Parsing article: {}'.format(response.url))
        rake = Rake()
        item = qdict(
            url = response.url,
            title = response.css('title::text').extract_first(),
            raw_body = ' '.join(response.css('.story-body__inner').css('p::text').extract()),
        )
        # import pdb;pdb.set_trace()
        rake.extract_keywords_from_text(item.raw_body)
        item.keywords = [ qdict(weight=i[0], value=i[1]) for i in rake.get_ranked_phrases_with_scores() ]
        # self.logger.debug('======>>> PARSED: {}\n: {}\n----END-OF-ARTICLE'.format(response.url, item))
        return item



