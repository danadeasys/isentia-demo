# encoding: utf-8
# package: isentia-demo
# author: Daniel Kovacs <danadeasysau@gmail.com>
# file-version: 1.0

# -------------------------------------------------------------------------------
# imports
# -------------------------------------------------------------------------------

from datetime import datetime

from ..articles import *

import scrapy


# -------------------------------------------------------------------------------
# exports
# -------------------------------------------------------------------------------



# -------------------------------------------------------------------------------
# MongoDBPipeline
# -------------------------------------------------------------------------------

class MongoDBPipeline(object):
    def process_item(self, item, spider):
        articles = Article.objects(url=item.url).all()
        if len(articles) >= 1:
            for article in articles:
                article.delete()
        keywords = item.pop('keywords', [])
        article = Article(**item)
        article.save()
        item['id'] = str(article.id)
        for kw in keywords:
            keyword = Keyword(article=article,**kw)
            keyword.save()
        item['keyword'] = keywords
        return item


